# GLI - AngularJS #

**BERNARD Tanguy**

### Objectif ###

L'objectif de ce tp est de développer un client en **AngularJS** qui exécutera des requêtes vers le service REST mis en place en [TAA](https://bitbucket.org/T4nguy/agile-app).

### Installation et exécution ###

1. Récupérer le projet
2. Exécuter: ```Grunt serve```

## Description ##

Application client en **AngularJs** ayant pour but de gérer un projet agile.
L'utilisateur a alors la possibilités de créer des epics, stories et tasks.
Il peut modifier une tache pour attribuer un utilisateur à cette dernière.

* Epics

![agile_epic.png](https://bitbucket.org/repo/Adn5b8/images/2071906342-agile_epic.png)

* Stories:

![agile_stories.png](https://bitbucket.org/repo/Adn5b8/images/899511040-agile_stories.png)

* Tasks:

![agile_tasks.png](https://bitbucket.org/repo/Adn5b8/images/467932786-agile_tasks.png)