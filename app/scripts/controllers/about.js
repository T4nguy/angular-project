'use strict';

/**
 * @ngdoc function
 * @name appAgileApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
