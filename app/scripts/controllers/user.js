'use strict';

/**
 * @ngdoc function
 * @name appAgileApp.controller:StoryCtrl
 * @description
 * # StoryCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')

  .controller('UserCtrl', function ($rootScope, $scope, $routeParams, $route,$http) {



    $scope.initFirst3 =function(){

      $http.get('/user/all').success(function (data) {
        $scope.users=data;
      });

    };

    $scope.initFirst3();


    $scope.submitForm = function($user){
      var data = {
        name: $user.name
      };

      $http.get('/user/create',
        {params:data})
        .success(function(data){
          $scope.myVariable=data;
          $scope.initFirst3();
        });

    };

    $scope.initFirst3();

  });
