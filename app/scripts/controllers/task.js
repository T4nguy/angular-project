'use strict';

/**
 * @ngdoc function
 * @name appAgileApp.controller:TaskCtrl
 * @description
 * # TaskCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')
  .controller('TaskCtrl', function ($rootScope, $scope, $routeParams, $route,$http, $timeout) {

    //If you want to use URL attributes before the website is loaded
    $rootScope.$on('$routeChangeSuccess', function () {
      console.log($routeParams.id_story);//id_story url parameter

    });

    $http.get('/task/get-by-story-id?id='+$routeParams.id_story)
      .then(function(response) {

        $timeout(function(){
          $scope.myTasks = response.data;
        },500);
        console.log("tasks all");



      });


    function upTask(){

      $http.get('/task/get-by-story-id?id='+$routeParams.id_story)
        .then(function(response) {

          $timeout(function(){
            $scope.myTasks = response.data;
          },500);




        });

    };

    $scope.getAllUsers =function(){

      $http.get('/user/all').success(function (data) {
        $scope.users=data;
      });

    };

    function addTask(task){
      var data = {
        id: $routeParams.id_story,
        name: task.name,
        status:task.status
      };

      $http.get('/task/create-by-story-id',
        {params:data})
        .success(function(data){

        });


    };


    $scope.submitForm = function(task){
      addTask(task);
      upTask();
    };

    $scope.delete_task = function (id_task) {
      var data = {
        id: id_task,
      };
      $http.get('/task/delete',
        {params:data})
        .then(function(data){
        });
      upTask();
    };


  });



/**
 * @ngdoc function
 * @name appAgileApp.controller:TaskModifCtrl
 * @description
 * # TaskModifCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')

  .controller('TaskModifCtrl', function ($rootScope, $scope, $routeParams, $route,$http) {

    //If you want to use URL attributes before the website is loaded
    $rootScope.$on('$routeChangeSuccess', function () {
      console.log($routeParams.id_story);//id_story url parameter

    });



    $scope.getAllUsers =function(){

      $http.get('/user/all').success(function (data) {
        $scope.users=data;
      });

    };


    $scope.modifForm = function(task){
      var data = {
        id: $routeParams.id_task,
        user_id: task.user_id,
        status:task.status
      };
      console.log(task.user_id);
      console.log(task.status);

      $http.get('/task/update',
        {params:data})
        .then(function(data){
          $scope.upTask;

        })
    };



    $http.get('/task/get?id='+$routeParams.id_task).success(function (data) {
      $scope.task =data;
      console.log(data);
    });

    $scope.getAllUsers();


  });
