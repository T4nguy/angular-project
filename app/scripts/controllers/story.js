'use strict';

/**
 * @ngdoc function
 * @name appAgileApp.controller:StoryCtrl
 * @description
 * # StoryCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')

  .controller('StoryCtrl', function ($rootScope, $scope, $routeParams, $route,$http, $timeout) {

    //If you want to use URL attributes before the website is loaded
    $rootScope.$on('$routeChangeSuccess', function () {
      console.log($routeParams.id_epic);//id_epic url parameter

    });


    $http.get('/story/get-by-epic-id?id='+$routeParams.id_epic).success(function (data) {
      $scope.stories=data;

    });

    function getStories(){
      $http.get('/story/get-by-epic-id?id='+$routeParams.id_epic).success(function (data) {
        $scope.stories=data;

      });

    };

    function addRowAction($story){
      var data = {
        id: $routeParams.id_epic,
        name: $story.name,
        effort: $story.effort,
        priority: $story.priority
      };


      $http.get('/story/create-by-epic-id',
        {params:data})
        ;

    };


    $scope.addRow = function($story){
      addRowAction($story);
      getStories();
    };



    $scope.delete_story = function ($id_story) {
      var data = {
        id: $id_story,
      };
      $http.get('/story/delete',
        {params:data})
        .then(function(data){
        });
      getStories();
    };



  });
