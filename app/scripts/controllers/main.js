'use strict';

/**
 * @ngdoc function
 * @name appAgileApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the appAgileApp
 */
angular.module('appAgileApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
