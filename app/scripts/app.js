'use strict';

/**
 * @ngdoc overview
 * @name appAgileApp
 * @description
 * # appAgileApp
 *
 * Main module of the application.
 */
angular
  .module('appAgileApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
        .when('/epic', {
          templateUrl: 'views/epic.html',
          controller: 'EpicCtrl',
          controllerAs: 'epic'
        })
      .when('/epic/:id_epic', {
        templateUrl: 'views/story.html',
        controller: 'StoryCtrl',
        controllerAs: 'story'
      })
      .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      })
      .when('/story/:id_story', {
        templateUrl: 'views/task.html',
        controller: 'TaskCtrl',
        controllerAs: 'task'
      })
      .when('/task/:id_task', {
        templateUrl: 'views/task_modif.html',
        controller: 'TaskModifCtrl',
        controllerAs: 'task'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
